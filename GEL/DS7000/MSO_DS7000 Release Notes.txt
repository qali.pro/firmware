
[Supported Model]    All the MSO/DS7000 Series Digital Oscilloscopes 
[Latest Revision Date] 2020/02/25

[Updated Contents]
--------------------

v00.01.02.00.05  2020/02/25
       
     - Optimized the connection HDMI start problem optimized the connection HDMI start problem
     - Optimize the vertical gear, channel zero elimination error 
     - Optimization of the inconsistency between SPI CLK and SDA names
     - Zoom mode square wave display in optimized 2S time base
     - Added command to get pass / fail times
     - Delete the default email account and password
     - Problems in remote instructions are optimized
     - Optimized 1K storage depth, waveform recording
     - The problem of too many stuck events in optimized decoding


v00.01.01.09.02  2019/07/23

     - Reduced the waveform noise floor
     - Optimized the waveform jitter caused by noise floor
     - Optimized the system response under the deep memory condition
     - Optimized the system response during the timebase adjustment process
     - Optimized the auto configuration condition for FFT waveform
     - Optimized the display effect during image saving
     - Modified the occasional problem of not displaying the measurement results
     - Optimized the user experience in cursor range adjustment in Measure setting
     - Modified relevant problems concerning SPI decoding
     - Modified address setting problems in I2C trigger
     - Added to save GPIB address setting during power-on storage
     - Modified the trigger sensitivity configuration. See Datasheet for details
     - Optimized the start-up time for the oscilloscope, shortening it to about
       1 minute

     - Added a command 
       :MEASure:STATistic:ITEM? CNT,<item>[,<src>[,<src>]]
     - Supported exporting the waveform memory data of several analog channels 
       simultaneously
     - Added high resolution mode
     - Supported mouse operation for cursor
     - Bandwidth upgrade option supported gradual upgrade
     - Added the 500 uV/div vertical scale

v00.01.01.07.01  2018/07/10

     - Auto supports unlocking by passwords
     - Math and Decode support displaying status and type
     - Add HELP menu item in "Utility->System"
     - Add probe ratios: 10000X,20000X,50000X
     - Fixed the problem of wave frozen
     - Fixed the exception of Fast acquisition
     - Fixed the setup saving and loading
     - Fixed the wave exporting and importing
     - Fixed the problem of Measure
     - Fixed the problem of Option uninstalling

v00.01.01.05.09  2018/05/03

     - Release the production version
