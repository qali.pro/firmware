#!/bin/sh -eu
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright 2019 (C) Olliver Schinagl <oliver@schinagl.nl>

set -eu

AES_KEY="BAD8CFFEBBAAB5C4C3D8D4BFCAFDBEDD"
GELDIR_TEMPLATE="gelunpacker"
KEEPGELDIR=0

PATH="${PATH}:/sbin:/usr/sbin:/usr/local/sbin"


e_err()
{
    >&2 echo "ERROR: ${*}"
}

e_warn()
{
    echo "WARN: ${*}"
}

usage()
{
    echo "Uage: ${0} [OPTIONS] FILE"
    echo "    -h  Print usage"
    echo "    -k  Keep temporary extracted files"
    echo "    -o  Output directory"
}

cleanup()
{
    if [ -d "${GELDIR:-}" ] && [ -z "${GELDIR##*${GELDIR_TEMPLATE}*}" ]; then
        if [ "${KEEPGELDIR}" -eq 1 ]; then
            e_warn "Not deleting '${GELDIR}', please remove manually."
        else
           rm -rf "${GELDIR:?}/"
        fi
    fi
}

main()
{
    while getopts ":hko:" options; do
        case "${options}" in
        h)
            usage
            exit 0
            ;;
        k)
            KEEPGELDIR=1
            ;;
        o)
            OUTDIR="${OPTARG}"
            ;;
        :)
            e_err "Option -${OPTARG} requires an argument."
            exit 1
            ;;
        ?)
            e_err "Invalid option: -${OPTARG}"
            exit 1
            ;;
        esac
    done
    shift "$((OPTIND - 1))"

    if [ ! -f "${1:-}" ]; then
        e_err "ERROR: Missing GEL file."
        usage
        exit 1
    fi

    case "${1}" in
    *"DS5000"*)
        DEVICE="kerstrel"
        ;;
    *"DS7000"*)
        DEVICE="flamingo"
        ;;
    *)
        DEVICE="unknown"
        ;;
    esac

    GELDIR="$(mktemp -d -t "${GELDIR_TEMPLATE}.XXXXXXXX")"
    if [ -z "${OUTDIR:-}" ]; then
        OUTDIR="${GELDIR}"
    fi
    mkdir -p "${OUTDIR}/rootfs/"

    tar xvf "${1}" -C "${GELDIR}"
    gunzip "${GELDIR}/"*".gz"
    mv "${GELDIR}/fw4linux.sh" "${GELDIR}/fw4linux.sh.aes-128"
    mv "${GELDIR}/fw4uboot.sh" "${GELDIR}/fw4uboot.sh.aes-128"
    cp -a "${GELDIR}/zynq.bit" "${OUTDIR}/zynq.bit"

    dumpimage -l "${GELDIR}/system.img"
    dumpimage -T flat_dt  "${GELDIR}/system.img" -p 0 -o "${OUTDIR}/zImage"
    dumpimage -T flat_dt  "${GELDIR}/system.img" -p 1 -o "${GELDIR}/${DEVICE}.dtb"
    dumpimage -T flat_dt  "${GELDIR}/system.img" -p 2 -o "${GELDIR}/initrd.ext2.img.gz"
    gunzip "${GELDIR}/initrd.ext2.img.gz"

    dtc -i dtb -O dts -o "${OUTDIR}/${DEVICE}.dts" "${GELDIR}/${DEVICE}.dtb"

    extract-ikconfig "${OUTDIR}/zImage" > "${OUTDIR}/${DEVICE}.config"

    debugfs -R 'ls -p' "${GELDIR}/initrd.ext2.img" | cut -d/ -f6 | while read entry; do
        debugfs -R "rdump \"${entry}\" ${OUTDIR}/rootfs/" "${GELDIR}/initrd.ext2.img"
    done
    rmdir "${OUTDIR}/rootfs/lost+found/" || true

    python3 /usr/share/ubidump/ubidump.py --preserve --savedir "${GELDIR}" "${GELDIR}/app.img"
    cp -ar "${GELDIR}/app/." "${OUTDIR}/rootfs/rigol/"

    for cryptfile in "fw4linux.sh" "fw4uboot.sh"; do
        openssl aes-128-cbc -in "${GELDIR}/${cryptfile}.aes-128" -out "${OUTDIR}/${cryptfile}" -d -K "${AES_KEY}" -iv "${AES_KEY}" -nopad
    done

    IMG_HEIGHT="$(printf %d "$(head -c 4 "${GELDIR}/logo.hex" | od -An -t d | tr -d '[:space:]')")"
    IMG_WIDTH="$(printf %d "$(head -c 8 "${GELDIR}/logo.hex" | tail -c 4 | od -An -t d | tr -d '[:space:]')")"
    tail -c +8 "${GELDIR}/logo.hex" | head -c "$((IMG_HEIGHT * IMG_WIDTH * 2))" > "${GELDIR}/logo.raw"
    ffmpeg -f rawvideo -pixel_format rgb565 -video_size "${IMG_HEIGHT}x${IMG_WIDTH}" -i "${GELDIR}/logo.raw" "${OUTDIR}/logo.png"

    echo "Successfully extracted '${1}'."
}

trap cleanup EXIT
main "${@}"

exit 0
