#!/bin/sh

export QTDIR=/rigol/Qt5.5
export PATH=$QTDIR:$PATH
export PATH=$PATH:/rigol/cups/sbin:/rigol/cups/bin
export QT_QPA_FONTDIR=$QTDIR/lib/fonts
export QT_QPA_PLATFORM=linuxfb:tty=/dev/fb0  
export QT_QPA_PLATFORM_PLUGIN_PATH=$QTDIR/plugins  

export TSDEVICE=/dev/input/event0
export QT_QPA_GENERIC_PLUGINS=evdevtouch:$TSDEVICE  

export LD_LIBRARY_PATH=/lib:/lib/rpc
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$QTDIR
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$QTDIR/lib/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$QT_QPA_FONTDIR
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$QT_QPA_PLATFORM_PLUGIN_PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/rigol/cups/lib

TOOLS=/rigol/tools
#for rmmod
#mkdir /lib/modules/$(uname -r)

############################################
#Install device driver
############################################
if [ ! -c /dev/axi ]; then
   mknod /dev/axi c 60 0
fi
insmod /rigol/drivers/axi.ko
insmod /rigol/drivers/devIRQ.ko
#insmod /rigol/drivers/adc128d818.ko
insmod /rigol/drivers/tmp421.ko

############################################
#fetch system information from bootloader
############################################
$TOOLS/cfger -i /tmp/sysinfo.txt

############################################
#Check if started by restoring default
############################################
$TOOLS/checkboot
if [ $? -eq 2 ]; then
	PowerOn="-nonv "
	echo '----------------Restore default'
fi

############################################
#Config Pll and K7
############################################
$TOOLS/spi2pll >/dev/null 2>&1
$TOOLS/beeper
#DOG
$TOOLS/creat

############################################
#Config K7  cost:8s
############################################
$TOOLS/spi2k7 >/dev/null 2>&1
if [ $? -ne 0 ]; then
	echo 'K7 Config failed'
else
	$TOOLS/beeper
fi

############################################
#Mount key data partition. cost:1s
############################################
/rigol/shell/mount_user_space.sh 0
$TOOLS/beeper

############################################
#Check default Calibration data
############################################
DATA_FILE=/rigol/data/lfcal.hex
if [ ! -f $DATA_FILE ]; then
	cp /rigol/default/* /rigol/data/
	sync
fi

#################Start app#######################
/rigol/appEntry $PowerOn  -run &


#MAC_ADDR=$(cat $MAC_FILE)
#ifconfig eth0 hw ether 00:19:AF:3f:01:2C
#ifconfig lo   127.0.0.1
#ifconfig eth0 up


#lxi
rpcbind >/dev/null 2>&1

############################################
#Start webcontrol service
############################################
DATA_FILE=/rigol/data/user.conf
if [ ! -f $DATA_FILE ]; then
	cp /rigol/default/user.conf /rigol/data/
	sync
fi
/rigol/webcontrol/sbin/lighttpd -f /rigol/webcontrol/config/lighttpd.conf &

############################################
#cups-Common Unix Print Service
############################################
/rigol/cups/sbin/cupsd &

############################################
#Mount user space partition
############################################
/rigol/shell/mount_user_space.sh 1 &

############################################
#Watdog
############################################
#$TOOLS/dog &


