#upgrade_shell
#check version first
if checkVer "00.01.01.04.08"; then \
	echo New version; \
else \
 echo Old version; \
 setenv bootparam "0x44454654"; \
fi;


#check all md5
showMessage 'Upgrading firmware,please waitting.';beeper;
if tar 0x4000000 0xA000000 logo.hex.gz; then \
	if md5sum -v 0xA000000 ${temp_file_size} 9cd0ee58489fb181899b306e3091544c; then \
	    echo check logo.hex.gz md5 success; \
	else \
	    echo check logo.hex.gz md5 error; \
    	setenv errmsg "Check md5 failed";\
		exit_from_shell;\
	fi; \
else \
	echo tar logo.hex.gz error !;\
  setenv errmsg "File parse failed(-1)";\
	exit_from_shell;\
fi;
if tar 0x4000000 0xA000000 zynq.bit.gz; then \
	if md5sum -v 0xA000000 ${temp_file_size} 28e69eaceacf4b65a5b620eb8aa84643; then \
	    echo check zynq.bit.gz md5 success; \
	else \
	    echo check zynq.bit.gz md5 error; \
    	setenv errmsg "Check md5 failed";\
		exit_from_shell;\
	fi; \
else \
	echo tar zynq.bit.gz error !;\
  setenv errmsg "File parse failed(-1)";\
	exit_from_shell;\
fi;
if tar 0x4000000 0xA000000 system.img.gz; then \
	if md5sum -v 0xA000000 ${temp_file_size} 7ce8e5933c88ea19357611ed0b69289e; then \
	    echo check system.img.gz md5 success; \
	else \
	    echo check system.img.gz md5 error; \
    	setenv errmsg "Check md5 failed";\
		exit_from_shell;\
	fi; \
else \
	echo tar system.img.gz error !;\
  setenv errmsg "File parse failed(-1)";\
	exit_from_shell;\
fi;
if tar 0x4000000 0xA000000 app.img.gz; then \
	if md5sum -v 0xA000000 ${temp_file_size} 8272155c0b2354ceeca4b670c05a4e3e; then \
	    echo check app.img.gz md5 success; \
	else \
	    echo check app.img.gz md5 error; \
    	setenv errmsg "Check md5 failed";\
		exit_from_shell;\
	fi; \
else \
	echo tar app.img.gz error !;\
  setenv errmsg "File parse failed(-1)";\
	exit_from_shell;\
fi;
#select app
if test ${bootpart} = A; then \
  echo update app2;\
	setenv log_offset '0xd500000'; \
	setenv bit_offset '0xd900000'; \
	setenv sys_offset '0xe100000'; \
	setenv app_offset '0x10100000'; \
	setenv bootpart 'B'; \
	setenv backpart 'A'; \
else \
  echo update app1;\
	setenv log_offset '0x4500000'; \
	setenv bit_offset '0x4900000'; \
	setenv sys_offset '0x5100000'; \
	setenv app_offset '0x7100000'; \
	setenv bootpart 'A'; \
	setenv backpart 'B'; \
fi;

#update
showMessage 'Upgrading firmware,please waitting..';beeper;
if tar 0x4000000 0xA000000 logo.hex.gz; then \
   if unzip 0xA000000 0x10000000; then \
      nand erase ${log_offset} 0x400000;\
      nand write 0x10000000 ${log_offset} ${temp_file_size};\
   else \
      echo unzip logo.hex.gz error !;\
  	setenv errmsg "File parse failed(-2)";\
		exit_from_shell;\
   fi; \
else \
	echo tar logo.hex.gz error !;\
	setenv errmsg "File parse failed(-3)"; \
	exit_from_shell;\
fi;
showMessage 'Upgrading firmware,please waitting...';beeper;
if tar 0x4000000 0xA000000 zynq.bit.gz; then \
   if unzip 0xA000000 0x10000000; then \
      nand erase ${bit_offset} 0x800000;\
      nand write 0x10000000 ${bit_offset} ${temp_file_size};\
   else \
      echo unzip zynq.bit.gz error !;\
  	setenv errmsg "File parse failed(-2)";\
		exit_from_shell;\
   fi; \
else \
	echo tar zynq.bit.gz error !;\
	setenv errmsg "File parse failed(-3)"; \
	exit_from_shell;\
fi;
showMessage 'Upgrading firmware,please waitting....';beeper;
if tar 0x4000000 0xA000000 system.img.gz; then \
   if unzip 0xA000000 0x10000000; then \
      nand erase ${sys_offset} 0x2000000;\
      nand write 0x10000000 ${sys_offset} ${temp_file_size};\
   else \
      echo unzip system.img.gz error !;\
  	setenv errmsg "File parse failed(-2)";\
		exit_from_shell;\
   fi; \
else \
	echo tar system.img.gz error !;\
	setenv errmsg "File parse failed(-3)"; \
	exit_from_shell;\
fi;
showMessage 'Upgrading firmware,please waitting.....';beeper;
if tar 0x4000000 0xA000000 app.img.gz; then \
   if unzip 0xA000000 0x10000000; then \
      nand erase ${app_offset} 0x6400000;\
      nand write 0x10000000 ${app_offset} ${temp_file_size};\
   else \
      echo unzip app.img.gz error !;\
  	setenv errmsg "File parse failed(-2)";\
		exit_from_shell;\
   fi; \
else \
	echo tar app.img.gz error !;\
	setenv errmsg "File parse failed(-3)"; \
	exit_from_shell;\
fi;
#env
setenv model   'MSO5074'
setenv softver '00.01.01.04.08'
setenv builddate '2019-08-02 13:33:26'

showMessage 'Upgrading firmware,please waitting......';beeper;
setenv nandboot${bootpart} "checkGTP;loadzynq ${bit_offset};ledoff;loadlogo ${log_offset};nand read 0x3000000 ${sys_offset} 0xd8ec38;bootm 0x3000000"
setenv bootlogo loadlogo ${log_offset}
setenv bootcmd   "if run nandboot${bootpart}; then echo ok; else setenv bootpart ${backpart};save;run nandboot${backpart}; fi"

#clear
setenv log_offset;
setenv bit_offset;
setenv sys_offset;
setenv app_offset;
setenv temp_file_size;
saveenv;
##end##










